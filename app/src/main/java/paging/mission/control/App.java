package paging.mission.control;

import static java.lang.ClassLoader.getSystemResource;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import paging.mission.control.models.AlertOutput;
import paging.mission.control.models.TelemetryInput;

public class App {

    // The number of minutes to look for repeat events
    public static final long THRESHOLD_WINDOW = 5;
    // The number of events to look for to trigger the threshold
    public static final int THESHOLD_AMOUNT = 3;
    // The expected Date Format

    public static void main(String[] args) {
        App app = new App();

        try {
            // Open a file from arguments if provided, otherwise use the included file
            File f;
            if(args.length==0) {
                f = new File(getSystemResource("input").toURI());
            } else {
                f = new File(args[0]);
            }
        
            // Process all Telementry and Generate Alerts
            List<AlertOutput> alerts = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(app.importFile(f), Spliterator.ORDERED), false)
                    .map(ti -> app.checkForAlert(ti))
                    .filter(x -> x != null)
                    .filter(x -> app.filterDesiredAlerts(x))
                    .collect(Collectors.toList());

            alerts = app.filterAlertsAboveThreshold(alerts);

            // Print Output
            System.out.println(app.convertToString(alerts));
        } catch(IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Takes in a '|' delimeted file and converts it into TelemetryInput objects
     * @param f A filename
     * @return An Iterator for the values in the File
     * @throws IOException
     */
    public MappingIterator<TelemetryInput> importFile(File f) throws IOException{
        CsvMapper cm = new CsvMapper();
        CsvSchema cs = cm.schemaFor(TelemetryInput.class)
                            .withColumnSeparator('|');
        MappingIterator<TelemetryInput> mi = cm.readerFor(TelemetryInput.class)
                                                .with(cs)
                                                .readValues(f);
        return mi;
    }

    /**
     * Checks each telemetry event on whether it should generate an alert
     * @param ti - A Telemetry Input Object
     * @return Either an AlertOutput Object it an alert was generated or null
     */
    public AlertOutput checkForAlert(TelemetryInput ti) {

        // Check to see if raw value is outside of the red or yellow limits
        String severity = null;
        if(ti.getRawValue()>ti.getRedHighLimit()){
            severity = "RED HIGH";
        } else if(ti.getRawValue()<ti.getRedLowLimit()){
            severity = "RED LOW";
        } else if(ti.getRawValue()>ti.getYellowHighLimit()){
            severity = "YELLOW HIGH";
        } else if(ti.getRawValue()<ti.getYellowLowLimit()){
            severity = "YELLOW LOW";
        } 

        // Create an AO object if an alert should be generated in this event
        AlertOutput ao = null;
        if(severity!=null) {
            ao = new AlertOutput();
            ao.setComponent(ti.getComponent());
            ao.setSatelliteId(ti.getSatelliteId());
            ao.setTimestamp(ti.getTimestamp());
            ao.setSeverity(severity);
        }
        return ao;
    }

    /**
     * This method will filter out any alerts that are not desired for the final output
     * @param ao - An AlertOutput object
     * @return True if the alert is desired in the final output and false otherwise
     */
    public boolean filterDesiredAlerts(AlertOutput ao) {
        // If there are thermostat readings that exceed the red high limit
        if(ao.getSeverity().equals("RED HIGH")&&ao.getComponent().equals("TSTAT")) {
            return true;
        }
        // If there battery voltage readings that are under the red low limit
        else if(ao.getSeverity().equals("RED LOW")&&ao.getComponent().equals("BATT")) {
            return true;
        }
        // All other alerts are undesired
        else {
            return false;
        }
    }

    /**
     * This will take in all the desired alerts, and return a filtered list of alerts that meet
     * the thresholds set for the application
     * @param l - A list of AlertOutput objects
     * @return A filtered list of AlertOutput Objects that meet the threshold
     */
    public List<AlertOutput> filterAlertsAboveThreshold(List<AlertOutput> l) {
        List<AlertOutput> filteredAlerts = new ArrayList<>();
        Map<String, List<AlertOutput>> buckets = new HashMap<>();

        // Iterate through each Alert
        for (AlertOutput ao: l) {
            // Create the key using the SatelliteID + Component
            String key = ao.getSatelliteId()+ao.getComponent();
            // Get the timestamp for the latest event
            Instant currentInstant = ao.getTimestamp();
            // Add this alert to its Sattelite+Component Bucket
            List<AlertOutput> bucket = buckets.get(key);
            // If this bucket of Sattellite + Component does not exist yet, create it and then Continue
            if(bucket==null) {
                buckets.put(key, new ArrayList<AlertOutput>(Collections.singletonList(ao)));
                continue;
            } 
            // If this bucket exists add the new entry to it
            else {
                bucket.add(ao);
            }

            // Calculate how many events happend in the last 5 minutes
            // New of events will increase the guage
            int guage =0;
            // Store the First Alert that is triggered in the threshold window
            AlertOutput firstAlert = null;
            for(int i=0; i<bucket.size(); i++) {
                Instant pastInstant = bucket.get(i).getTimestamp();
                if(pastInstant.isAfter(currentInstant.minus(THRESHOLD_WINDOW, ChronoUnit.MINUTES))) {
                    guage++;

                    // Capture the first alert that was triggered in the threshold window
                    if(firstAlert==null) {
                        firstAlert = bucket.get(i);
                    }
                }
            }
            // If that Guage is above the threshold for the alert
            // Add the first instance of the triggered alert in the window to the filtered Alerts
            if(guage>=THESHOLD_AMOUNT) {
                filteredAlerts.add(firstAlert);
            }
        }
        return filteredAlerts;
    }
    
    /**
     * Converts a list of alerts to a JSON string
     * @param alerts - A list of Alerts
     * @return A JSON string representing the alerts
     * @throws JsonProcessingException
     */
    public String convertToString(List<AlertOutput> alerts) throws JsonProcessingException{
        ObjectMapper om = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        return om.writeValueAsString(alerts);
    }
}
