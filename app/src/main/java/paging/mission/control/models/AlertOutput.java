package paging.mission.control.models;

import java.time.Instant;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import paging.mission.control.utils.InstantSerializer;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlertOutput {

    private int satelliteId;
    private String severity;
    private String component;
    @JsonSerialize(using = InstantSerializer.class)
    private Instant timestamp;
  
}
