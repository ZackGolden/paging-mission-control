package paging.mission.control.models;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import paging.mission.control.utils.InstantDeserializer;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder( {"timestamp", "satelliteId", "redHighLimit", "yellowHighLimit", "yellowLowLimit", "redLowLimit", "rawValue", "component"})
public class TelemetryInput {

// <timestamp>
@JsonDeserialize(using = InstantDeserializer.class)
private Instant timestamp;

// <satellite-id>
private int satelliteId;

// <red-high-limit>
private double redHighLimit;

// <yellow-high-limit>
private double yellowHighLimit; 

// <yellow-low-limit> 
private double yellowLowLimit; 

// <red-low-limit>
private double redLowLimit;

// <raw-value>
private double rawValue;

// <component>
private String component;

}