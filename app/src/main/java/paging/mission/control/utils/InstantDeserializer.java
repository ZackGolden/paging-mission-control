package paging.mission.control.utils;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class InstantDeserializer extends StdDeserializer<Instant>{

  public InstantDeserializer() {
    this(Instant.class);
  }

  protected InstantDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  /**
   * This Method contains the special logic to Convert the String in the input file to a timestamp
   */
  public Instant deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    DateTimeFormatter f = DateTimeFormatter
            .ofPattern("yyyyMMdd HH:mm:ss.SSS")
            .withLocale(Locale.getDefault())
            .withZone(ZoneId.of("Z"));;
    return Instant.from(f.parse(p.getText()));
  }
  
}
