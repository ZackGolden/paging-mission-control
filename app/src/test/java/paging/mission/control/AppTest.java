package paging.mission.control;

import static java.lang.ClassLoader.getSystemResource;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import paging.mission.control.models.AlertOutput;
import paging.mission.control.models.TelemetryInput;

class AppTest {
    App app = new App();

    @Tag("checkForAlert")
    @DisplayName("Generate RED HIGH")
    @Test void validateCheckForAlert1() {
        TelemetryInput ti = new TelemetryInput(Instant.MIN, 1000, 101, 98, 25, 20, 102.9, "TSTAT");
        AlertOutput ao = app.checkForAlert(ti);
        assertTrue(ao.getSeverity().equals("RED HIGH"), "This should have generated an alert with Severity RED HIGH");
    }

    @Tag("checkForAlert")
    @DisplayName("Generate YELLOW HIGH")
    @Test void validateCheckForAlert2() {
        TelemetryInput ti = new TelemetryInput(Instant.MIN, 1001, 101, 98, 25, 20, 99.8, "TSTAT");
        AlertOutput ao = app.checkForAlert(ti);
        assertTrue(ao.getSeverity().equals("YELLOW HIGH"), "This should have generated an alert with Severity YELLOW HIGH");
    }
        
    @Tag("checkForAlert")
    @DisplayName("Generate YELLOW LOW")
    @Test void validateCheckForAlert3() {
        TelemetryInput ti = new TelemetryInput(Instant.MIN, 1001, 101, 98, 25, 20, 23, "TSTAT");
        AlertOutput ao = app.checkForAlert(ti);
        assertTrue(ao.getSeverity().equals("YELLOW LOW"), "This should have generated an alert with Severity YELLOW LOW");
    }

    @Tag("checkForAlert")
    @DisplayName("Generate RED LOW")
    @Test void validateCheckForAlert4() {
        TelemetryInput ti = new TelemetryInput(Instant.MIN, 1001, 101, 98, 25, 20, 19, "TSTAT");
        AlertOutput ao = app.checkForAlert(ti);
        assertTrue(ao.getSeverity().equals("RED LOW"), "This should have generated an alert with Severity RED LOW");
    }

    @Tag("checkForAlert")
    @DisplayName("Generate nothing")
    @Test void validateCheckForAlert5() {
        TelemetryInput ti = new TelemetryInput(Instant.MIN, 1001, 101, 98, 25, 20, 50, "TSTAT");
        AlertOutput ao = app.checkForAlert(ti);
        assertNull(ao, "This should not have generated an alert");
    }

    @Tag("filterDesiredAlerts")
    @DisplayName("A Desired Alert returns true")
    @Test void validateFilterDesiredAlerts1() {
        AlertOutput ao = new AlertOutput();
        ao.setComponent("BATT");
        ao.setSeverity("RED LOW");
        boolean result = app.filterDesiredAlerts(ao);
        assertTrue(result, "A Desired alert should return true");

    }
    
    @Tag("filterDesiredAlerts")
    @DisplayName("An undesired Alert returns False")
    @Test void validateFilterDesiredAlerts2() {
        AlertOutput ao = new AlertOutput();
        ao.setComponent("BATT");
        ao.setSeverity("YELLOW LOW");
        boolean result = app.filterDesiredAlerts(ao);
        assertFalse(result, "An undesired alert should return false");

    }

    @Tag("filterAlertsAboveThreshold")
    @DisplayName("Return an Alert that meets the threshold")
    @Test void validateFilterAlertsAboveThreshold1() {
        List<AlertOutput> l = new ArrayList<>();
        l.add(new AlertOutput(1000, "RED HIGH", "TSTAT", Instant.parse("2018-01-02T05:03:05.009Z")));
        l.add(new AlertOutput(1000, "RED HIGH", "TSTAT", Instant.parse("2018-01-02T05:04:05.009Z")));
        l.add(new AlertOutput(1000, "RED HIGH", "TSTAT", Instant.parse("2018-01-02T05:05:05.009Z")));
        List<AlertOutput> results = app.filterAlertsAboveThreshold(l);
        assertTrue(results.size()==1, "Only one event should have been retured");
    }

    @Tag("filterAlertsAboveThreshold")
    @DisplayName("Return no alerts if nothing meets the threshold")
    @Test void validateFilterAlertsAboveThreshold2() {
        List<AlertOutput> l = new ArrayList<>();
        l.add(new AlertOutput(1000, "RED HIGH", "TSTAT", Instant.parse("2018-01-02T05:03:05.009Z")));
        l.add(new AlertOutput(1001, "RED HIGH", "TSTAT", Instant.parse("2018-01-02T05:04:05.009Z")));
        l.add(new AlertOutput(1000, "RED HIGH", "TSTAT", Instant.parse("2018-01-02T05:05:05.009Z")));
        List<AlertOutput> results = app.filterAlertsAboveThreshold(l);
        assertTrue(results.size()==0, "No events should have been retured");
    }

    @Tag("convertToString")
    @DisplayName("Validate that The Correct String is generated")
    @Test void validateConvertToString() throws JsonProcessingException {
        List<AlertOutput> alerts = Collections.singletonList( new AlertOutput(1000, "RED HIGH", "TSTAT", Instant.parse("2018-01-02T05:05:05.009Z")));
        String actualOutput = app.convertToString(alerts);
        String expectedOutput = "[ {\n"
            + "  \"satelliteId\" : 1000,\n"
            + "  \"severity\" : \"RED HIGH\",\n"
            + "  \"component\" : \"TSTAT\",\n"
            + "  \"timestamp\" : \"2018-01-02T05:05:05.009Z\"\n"
            +"} ]";
        assertTrue(actualOutput.equals(expectedOutput), "The Generated String Does not match");
    }

    @Tag("importFile")
    @DisplayName("Validate that the inputFile is read")
    @Test void validateImportFile() throws IOException, URISyntaxException {
        File f = new File(getSystemResource("input").toURI());
        MappingIterator<TelemetryInput> mi = app.importFile(f);
        TelemetryInput ti = mi.next();
        assertEquals(Instant.parse("2018-01-01T23:01:05.001Z"), ti.getTimestamp(), "Timestamp Does not Match");
        assertEquals(1001, ti.getSatelliteId(), "Satellite ID Does not Match");
        assertEquals(101, ti.getRedHighLimit(), "Red High Limit Does not Match");
        assertEquals(98, ti.getYellowHighLimit(), "Yellow High Limit Does not Match");
        assertEquals(25, ti.getYellowLowLimit(), "Yellow Low Limit Does not Match");
        assertEquals(20, ti.getRedLowLimit(), "Red Low Limit Does not Match");
        assertEquals(99.9, ti.getRawValue(), "Raw Value Does not Match");
        assertEquals("TSTAT", ti.getComponent(), "Component Does not Match");
    } 
}